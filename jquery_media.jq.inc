<?php

/**
 *  Callback for jquery_media_jq
 */
function _jquery_media_jq($op, $plugin = NULL, $options = array()) {
  switch ($op) {
    case 'info':
      return array(
          'jquery_media' => array(
              'name' => t('jQuery Media'),
              'description' => t('By M. Alsup. The jQuery Media Plugin supports unobtrusive conversion of standard markup into rich media content. It can be used to embed virtually any media type, including Flash, Quicktime, Windows Media Player, Real Player, MP3, Silverlight, PDF and more, into a web page. The plugin converts an element (usually a link) into a div which holds the object, embed or iframe tags neccessary to render the media content.'),
              'version' => '0.75 (02/20/2008)',
              'url' => 'http://www.malsup.com/jquery/media/',
              'invocation' => t('You may invoke this plugin with %code. You may further set additional default parameters by passing an array, such as with %array (which will override the defaults on this screen). See !help for more information.', array('%code' => "jq_add('jquery_media')", '%array' => "jq_add('jquery_media', array('bgcolor' => '#660000', 'autoplay' => TRUE))", '!help' => l(t('jQuery Media help'), 'admin/help/jquery_media'))),
              'files' => array(
                  'js' => array(
                      drupal_get_path('module', 'jquery_media') .'/js/jquery.media.js',
                  ),
              ),
          ),
      );
      break;
    case 'add':
      _jquery_media_add($options);
      break;
    case 'settings':
      include_once(drupal_get_path('module', 'jquery_media') .'/jquery_media.settings.inc');
      return _jquery_media_settings_form();
      break;
  }
}

function _jquery_media_add($options = array(), $return_js = FALSE) {
  static $defaults = array();
  static $jquery_blockui_installed = FALSE;

  $options = (array)$options;
  if (empty($defaults) && !$return_js && variable_get('jquery_media_use_default_js_file', JQUERY_MEDIA_USE_DEFAULT_JS_FILE)) {
    drupal_add_js(variable_get('jquery_media_default_js_filepath', JQUERY_MEDIA_DEFAULT_JS_FILEPATH));
    $default_js_loaded = TRUE;
  }

  if (empty($defaults) || !empty($options)) {
    if ($swf = variable_get('jquery_media_swfobject_path', '')) {
      drupal_add_js($swf);
    }
    // change jQuery Media's defaults. these may be overrriden on an individual basis.
    if (!($default_js_loaded || isset($defaults['flvplayer'])) || $options['flvplayer']) {
      $defaults['flvplayer'] = $options['flvplayer'] ? url($options['flvplayer']) : url(variable_get('jquery_media_flvplayer', JQUERY_MEDIA_FLVPLAYER_DEFAULT));
      $js .= "    $.fn.media.defaults.flvPlayer = '{$defaults['flvplayer']}';\n";
    }
    if (!($default_js_loaded || isset($defaults['mp3player'])) || $options['mp3player']) {
      $defaults['mp3player'] = $options['mp3player'] ? url($options['mp3player']) : url(variable_get('jquery_media_mp3player', JQUERY_MEDIA_MP3PLAYER_DEFAULT));
      $js .= "    $.fn.media.defaults.mp3Player = '{$defaults['mp3player']}';\n";
    }
    if (!($default_js_loaded || isset($defaults['boxtitle'])) || $options['boxtitle']) {
      $defaults['boxtitle'] = $options['boxtitle'] ? check_plain($options['boxtitle']) : check_plain(variable_get('jquery_media_boxtitle', ''));
      if ($defaults['boxtitle']) {
        $js .= "    $.fn.media.defaults.boxTitle = '{$defaults['boxtitle']}';\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['loadingimage'])) || $options['loadingimage']) {
      $defaults['loadingimage'] = $options['loadingimage'] ? $options['loadingimage'] : variable_get('jquery_media_loadingimage', '');
      if ($defaults['loadingimage']) {
        $defaults['loadingimage'] = url($defaults['loadingimage']);
        $js .= "    $.fn.media.defaults.loadingImage = '{$defaults['loadingimage']}';\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['autoplay'])) || isset($options['autoplay'])) {
      $defaults['autoplay'] = $options['autoplay'] ? $options['autoplay'] : variable_get('jquery_media_autoplay', JQUERY_MEDIA_AUTOPLAY_DEFAULT);
      if ($defaults['autoplay'] != JQUERY_MEDIA_AUTOPLAY_DEFAULT) {
        $js .= "    $.fn.media.defaults.autoplay = {$defaults['autoplay']};\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['bgcolor'])) || $options['bgcolor']) {
      $defaults['bgcolor'] = $options['bgcolor'] ? $options['bgcolor'] : variable_get('jquery_media_bgcolor', JQUERY_MEDIA_BGCOLOR_DEFAULT);
      if ($defaults['bgcolor'] != JQUERY_MEDIA_BGCOLOR_DEFAULT) {
        $js .= "    $.fn.media.defaults.bgColor = '{$defaults['bgcolor']}';\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['flashvars'])) || $options['flashvars']) {
      $defaults['flashvars'] = $options['flashvars'] ? $options['flashvars'] : variable_get('jquery_media_flashvars', '');
      if ($defaults['flashvars']) {
        $js .= "    $.fn.media.defaults.flashvars = { {$defaults['flashvars']} };\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['params'])) || $options['params']) {
      $defaults['params'] = $options['params'] ? $options['params'] : variable_get('jquery_media_params', '');
      if ($defaults['params']) {
        $js .= "    $.fn.media.defaults.params = { {$defaults['params']} };\n";
      }
    }
    if (!($default_js_loaded || isset($defaults['invoke media'])) || $options['invoke media'] || $options['media class'] || $options['media width'] || $options['media height']) {
      $defaults['invoke media'] = $options['invoke media'] ? $options['invoke media'] : variable_get('jquery_media_class_media_autoinvoke', JQUERY_MEDIA_CLASS_MEDIA_AUTOINVOKE_DEFAULT);
      if ($defaults['invoke media']) {
        $defaults['media class'] = $options['media class'] ? $options['media class'] : variable_get('jquery_media_class_media', JQUERY_MEDIA_CLASS_MEDIA_DEFAULT);
        $defaults['media width'] = $options['media width'] ? $options['media width'] : variable_get('jquery_media_media_width', JQUERY_MEDIA_MEDIA_WIDTH_DEFAULT);
        $defaults['media height'] = $options['media height'] ? $options['media height'] : variable_get('jquery_media_media_height', JQUERY_MEDIA_MEDIA_HEIGHT_DEFAULT);
        $size = array();
        if (!($defaults['media width'] === '')) {
          $size[] = 'width: '. $defaults['media width'];
        }
        if (!($defaults['media height'] === '')) {
          $size[] = 'height: '. $defaults['media height'];
        }
        $list = implode(', ', $size);
        $list = $list ? " { $list } " : '';
        if ($defaults['media class']) {
          $js .= "    $('{$defaults['media class']}').media($list);\n";
        }
      }
    }
    if (!($default_js_loaded || isset($defaults['invoke mediabox'])) || $options['invoke mediabox'] || $options['mediabox class']) {
      $defaults['invoke mediabox'] = $options['invoke mediabox'] ? $options['invoke mediabox'] : variable_get('jquery_media_class_mediabox_autoinvoke', JQUERY_MEDIA_CLASS_MEDIABOX_AUTOINVOKE_DEFAULT);
      if ($defaults['invoke mediabox']) {
        _jquery_media_add_blockui();
        $defaults['mediabox class'] = $options['mediabox class'] ? $options['mediabox class'] : variable_get('jquery_media_class_mediabox', JQUERY_MEDIA_CLASS_MEDIABOX_DEFAULT);
        $defaults['mediabox width'] = $options['mediabox width'] ? $options['mediabox width'] : variable_get('jquery_media_mediabox_width', JQUERY_MEDIA_MEDIABOX_WIDTH_DEFAULT);
        $defaults['mediabox height'] = $options['mediabox height'] ? $options['mediabox height'] : variable_get('jquery_media_mediabox_height', JQUERY_MEDIA_MEDIABOX_HEIGHT_DEFAULT);
        $size = array();
        if (!($defaults['mediabox width'] === '')) {
          $size[] = 'width: '. $defaults['mediabox width'];
        }
        if (!($defaults['mediabox height'] === '')) {
          $size[] = 'height: '. $defaults['mediabox height'];
        }
        $list = implode(', ', $size);
        $list = $list ? " { $list } " : '';
        if ($defaults['mediabox class']) {
          $js .= "    $('{$defaults['mediabox class']}').mediabox($list);\n";
        }
      }
    }
    if ($js) {
      $js = $js = "\nif (Drupal.jsEnabled) {\n  $(document).ready(function() {\n$js  });  \n}\n";
    }
    if ($return_js == TRUE) {
      return $js;
    }
    if ($js) {
      drupal_add_js($js, 'inline');
    }
  }
}

/**
 *  Install the jQuery BoxUI plugin. This is required for mediabox. We attempt to invoke it through jq and jquery_blockui first,
 *  to make sure we have the most recent version available. As a fallback, it's included with this module.
 */
function _jquery_media_add_blockui() {
  static $jquery_blockui_installed;
  if (!$jquery_blockui_installed) {
    if (!($jquery_blockui_installed = module_invoke('jq', 'add', 'jquery_blockui'))) {
      if (!($jquery_blockui_installed = module_invoke('jquery_blockui', 'add'))) {
        drupal_add_js(drupal_get_path('module', 'jquery_media') .'/js/jquery.blockUI.js');
        $jquery_blockui_installed = TRUE;
      }
    }
  }
  return TRUE;
}
